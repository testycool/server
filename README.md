# TestyCool Server

This project is currently WIP and under heavy development.

Executable documentation is available with `--help` flag:

```console
$ testycool-server --help
Usage:
  testycool-server [OPTIONS]

Application Options:
      --passcode=          Passcode to access admin privileges on this server [$ADMIN_PASSCODE]
      --verbose            Enable verbose debug information and show request log (if enabled) to stdout
      --data-dir=          Directory to store runtime data (default: data)
      --database=[sqlite]  Database to use (default: sqlite)
  -v, --version            Show version and exit

gRPC Options:
  -H, --grpc-host=         Address for gRPC to listen on (default: localhost) [$GRPC_HOST]
  -p, --grpc-port=         gRPC listening port (default: 50051) [$GRPC_PORT]
      --grpc-reflection    Enable gRPC reflection service
      --grpc-log-requests  Enable gRPC requests logging [$GRPC_LOG_REQUESTS]

SQLite Options:
      --sqlite-pool-size=  SQLite connection pool count (default: 1000) [$SQLITE_POOL_SIZE]

Help Options:
  -h, --help               Show this help message
```

As per `--help` flag, administrator passcode can also be set via environment variable `ADMIN_PASSCODE`. If both are set,
the environment variable takes precedence. Also, if neither is set, the default passcode will be randomly generated.

## Building

Tools for building the binaries:

* Go version 1.17.x
* GNU Make version 4.3+
* Bash or Bash compatible shell e.g. Zsh

Setup project dependencies for building:

```console
$ make setup
```

Build the project:

```console
$ make
```

Binary will be stored in `dist/` directory. The resulting binary is development build version without stripping and
compression for development and testing. See [Packaging](#packaging) to build with binary stripping and compression for
distribution.

To build for different platform, set `GOOS` and `GOARCH` environment variables to target platform. For example, if the
target is Windows x86_64:

```console
$ GOOS=windows GOARCH=amd64 make
```

Supported platforms:

| GOOS      | GOARCH   |
|-----------|----------|
| darwin    | amd64    |
| darwin    | arm64    |
| linux     | 386      |
| linux     | amd64    |
| linux     | arm      |
| linux     | arm64    |
| windows   | amd64    |

## Packaging

Tools for packaging:

* Tools for [building](#building)
* Git
* [UPX](https://upx.github.io/) for executable compression
* (Optional) Docker for building docker image

Build and package the project for single target:

```console
$ make package
```

Example to package for different target:

```console
$ GOOS=windows GOARCH=amd64 make package
```

Artifacts will be stored in `dist/` directory.

## Development

To see Makefile targets documentation:

```console
$ make help
```

Required tools for development:

* [protoc](https://grpc.io/docs/protoc-installation/) for compiling protocol buffers
* [GoMock](https://github.com/golang/mock) version 1.6.0 for generating mocks
* [goimports](https://pkg.go.dev/golang.org/x/tools/cmd/goimports) for consistent import order
* [golangci-lint](https://github.com/golangci/golangci-lint) version 1.42.0 for linting

```console
# Check development tools setup
$ make setup-check
```

```console
# Install development tools
$ make setup-dev
```

## Project Structure

* [`api/`](api) \
  Protocol buffer definitions, OpenAPI specs, and other information for external client.
    * [`api/protobuf/`](api/protobuf) \
      Protocol buffer definitions.
* [`build/`](build) \
  Packaging and Continuous Integration. Primarily Dockerfile(s).
* [`cmd/`](cmd) \
  Applications entrypoint. `main` package(s) should be defined here.
* [`db/`](db) \
  SQL migrations, queries, entrypoint, etc.
* [`internal/`](internal) \
  Private application and library code.

## TODO

### Domain

- [x] Authentication
    - [x] Auth token creation from credentials
    - [x] Admin passcode from command line argument
    - [ ] **Unit tests**
- [x] Exam
    - [x] Create new exam
    - [x] Get one exam by ID
    - [x] Get one exam by Password
    - [x] List all exams (with pagination)
    - [x] Update existing exam
    - [x] Delete existing exam
    - [ ] **Unit tests**
- [ ] Question and Option
    - [ ] Create new exam question with its options
    - [ ] Get one question with its options by question ID
    - [ ] List all questions with options for an exam (with pagination)
    - [ ] Update existing question
    - [ ] Delete existing question
    - [ ] **Unit tests**
- [ ] Participant
    - [x] Create new exam participant
    - [x] Get one participant by ID
    - [x] List all participants for an exam (with pagination)
    - [ ] List waiting participant in realtime (with pagination)
    - [x] Update existing participant
    - [x] Delete existing participant
    - [ ] **Unit tests**

### Data Persistence

- [x] Auth SQLite
- [x] Exam SQLite
- [ ] Question SQLite
- [ ] Option SQLite
- [x] Participant SQLite

### User Interface

- [ ] gRPC Server
    - [x] Authentication service server
    - [x] Exam service server
    - [ ] Question service server
    - [x] Participant service server

### Other

- [x] JWT service wrapper package
- [ ] Release artifacts file server
- [ ] GitLab CI pipeline

## License

`testycool-server` is released under the Apache 2.0 license. See [LICENSE](LICENSE) for details.