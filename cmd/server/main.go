// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"embed"
	"errors"
	"fmt"
	"net"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	flags "github.com/jessevdk/go-flags"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/analytics"
	"gitlab.com/testycool/server/internal/answer"
	"gitlab.com/testycool/server/internal/attempt"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/choice"
	"gitlab.com/testycool/server/internal/config"
	"gitlab.com/testycool/server/internal/datastore/sqlite"
	"gitlab.com/testycool/server/internal/exam"
	"gitlab.com/testycool/server/internal/jwt"
	"gitlab.com/testycool/server/internal/participant"
	"gitlab.com/testycool/server/internal/question"
	"gitlab.com/testycool/server/internal/transport/grpc"
	"gitlab.com/testycool/server/internal/version"
	"gitlab.com/testycool/server/internal/webui"
)

//nolint:lll // line length for clear description
type options struct {
	Passcode     string        `long:"passcode" description:"Passcode to access admin privileges on this server" env:"ADMIN_PASSCODE"`
	Verbose      bool          `long:"verbose" description:"Enable verbose debug information and show request log (if enabled) to stdout"`
	DataDir      string        `long:"data-dir" description:"Directory to store runtime data" default:"data"`
	Database     string        `long:"database" description:"Database to use" choice:"sqlite" default:"sqlite"`
	PrintVersion func()        `short:"v" long:"version" description:"Show version and exit"`
	GRPC         grpcOptions   `group:"gRPC Options" namespace:"grpc" env-namespace:"GRPC"`
	SQLite       sqliteOptions `group:"SQLite Options" namespace:"sqlite" env-namespace:"SQLITE"`
}

//nolint:lll // line length for clear description
type grpcOptions struct {
	Host        ipAddr `short:"H" long:"host" description:"Address for gRPC to listen on" default:"localhost" env:"HOST"`
	Port        int    `short:"p" long:"port" description:"gRPC listening port" default:"50051" env:"PORT"`
	Reflection  bool   `long:"reflection" description:"Enable gRPC reflection service"`
	LogRequests bool   `long:"log-requests" description:"Enable gRPC requests logging" env:"LOG_REQUESTS"`
	CertFile    string `long:"cert-file" description:"TLS Certificate file path"`
	KeyFile     string `long:"key-file" description:"TLS Key file path"`
}

type sqliteOptions struct {
	PoolSize int `long:"pool-size" description:"SQLite connection pool count" default:"10" env:"POOL_SIZE"`
}

type ipAddr string

func (i ipAddr) String() string {
	return string(i)
}

func (i ipAddr) IsValidValue(value string) error {
	if ip := net.ParseIP(value); ip == nil {
		return fmt.Errorf("invalid IP address: %s", value)
	}
	return nil
}

// migrationFS is file system for migration files that is embedded into the binary.
//go:generate ../../script/copy-migrations.sh migrations
//go:embed migrations/*.sql
var migrationFS embed.FS

//nolint:funlen // function length
func main() {
	var opts options
	opts.PrintVersion = func() {
		//nolint:forbidigo // fmt.Printf call
		fmt.Printf("v%s %s %s %s\n", version.Version, version.Branch, version.GitSHA[:7], version.BuildTimestamp)
		os.Exit(0)
	}

	// parse arguments
	parser := flags.NewParser(&opts, flags.Default)
	parser.NamespaceDelimiter = "-"
	_, err := parser.Parse()
	if err != nil {
		if errors.Is(err, flags.ErrRequired) {
			//nolint:forbidigo // fmt.Println call
			fmt.Println("use -h or --help to see all available options")
		}
		os.Exit(1)
	}

	// create server configuration
	cfg := &config.Config{
		GRPC: config.GRPC{
			Host:             opts.GRPC.Host.String(),
			Port:             opts.GRPC.Port,
			EnableReflection: opts.GRPC.Reflection,
			LogRequests:      opts.GRPC.LogRequests,
			CertFile:         opts.GRPC.CertFile,
			KeyFile:          opts.GRPC.KeyFile,
		},
		Runtime: config.Runtime{
			DataDir: opts.DataDir,
			LogDir:  filepath.Join(opts.DataDir, "log"),
		},
		SQLite: config.SQLite{
			DBFileName: "db.sqlite",
			PoolSize:   opts.SQLite.PoolSize,
		},
		Verbose: opts.Verbose,
	}

	// setup general logger
	logger := newServerLogger(cfg)
	panicIfError := func(err error) {
		if err != nil {
			logger.Panic(err)
		}
	}

	// set runtime and create directories if not exist
	err = setAndCreateRuntimeDir(cfg)
	panicIfError(err)

	logFile, err := addLoggerFileHook(logger, cfg)
	panicIfError(err)
	defer func() {
		errClose := logFile.Close()
		if errClose != nil {
			logger.Errorf("unable to close log file: %v", err)
		}
	}()

	go func() {
		w, err := webui.NewWebUI(logger.WithField("ui", "webui"), cfg)
		panicIfError(err)
		if serveErr := w.Serve(); serveErr != nil {
			panicIfError(serveErr)
		}
	}()

	logger.WithFields(logrus.Fields{
		"pid": os.Getpid(),
	}).Info("Server is starting")

	// create sqlite connection pool
	dbPool, err := sqlite.NewPool(cfg)
	panicIfError(err)
	defer func() {
		errClose := dbPool.Close()
		if errClose != nil {
			logger.Errorf("unable to close sqlite connection pool: %v", err)
		}
	}()

	// perform database migration
	err = sqlite.Migrate(dbPool, migrationFS)
	panicIfError(err)

	// JWT key initialization
	jwtKeyPath := filepath.Join(cfg.Runtime.DataDir, "jwt")
	jwtKey, err := jwt.GetRSAKey(logger, jwtKeyPath+".key", jwtKeyPath+".pub")
	panicIfError(err)

	// provide random passcode if not set
	if opts.Passcode == "" {
		opts.Passcode = auth.GenerateRandomPasscode(8)
		logger.Warningf("Passcode is not set, using random passcode: %s", opts.Passcode)
	}

	// store setup
	credStore, err := auth.NewSQLiteStore(logger, dbPool, opts.Passcode)
	panicIfError(err)
	examStore, err := exam.NewSQLiteStore(logger, dbPool)
	panicIfError(err)
	participantStore, err := participant.NewSQLiteStore(logger, dbPool)
	panicIfError(err)
	questionStore, err := question.NewSQLiteStore(logger, dbPool)
	panicIfError(err)
	choiceStore, err := choice.NewSQLiteStore(logger, dbPool)
	panicIfError(err)
	answerStore, err := answer.NewSQLiteStore(logger, dbPool)
	panicIfError(err)
	attemptStore, err := attempt.NewSQLiteStore(logger, dbPool)
	panicIfError(err)
	analyticsStore, err := analytics.NewSQLiteStore(logger, dbPool)
	panicIfError(err)

	// service setup
	const serviceLogKey = "service"
	jwtService, err := jwt.NewService(jwtKey)
	panicIfError(err)
	authService, err := auth.NewService(logger, jwtService, credStore)
	panicIfError(err)
	examService, err := exam.NewService(logger, examStore)
	panicIfError(err)
	participantService, err := participant.NewService(logger, participantStore)
	panicIfError(err)
	questionService, err := question.NewService(logger.WithField(serviceLogKey, "question"), questionStore)
	panicIfError(err)
	choiceService, err := choice.NewService(logger.WithField(serviceLogKey, "choice"), choiceStore)
	panicIfError(err)
	answerService, err := answer.NewService(logger.WithField(serviceLogKey, "answer"), answerStore)
	panicIfError(err)
	attemptService, err := attempt.NewService(logger.WithField(serviceLogKey, "attempt"), attemptStore)
	panicIfError(err)
	analyticsService, err := analytics.NewService(logger.WithField(serviceLogKey, "analytics"), analyticsStore)
	panicIfError(err)

	// transport setup
	const transportLogKey = "transport"

	// grpc
	grpcServices := &grpc.ServiceContainer{
		JWTService:         jwtService,
		AuthService:        authService,
		ExamService:        examService,
		ParticipantService: participantService,
		QuestionService:    questionService,
		ChoiceService:      choiceService,
		AnswerService:      answerService,
		AttemptService:     attemptService,
		AnalyticsService:   analyticsService,
	}
	grpcTransport, err := grpc.NewTransport(logger.WithField(transportLogKey, "grpc"), cfg, grpcServices)
	panicIfError(err)

	// start transports in goroutines
	errChan := make(chan error, 1)
	go func() {
		if serveErr := grpcTransport.Serve(); serveErr != nil {
			errChan <- serveErr
		}
	}()

	select {
	case <-grpcTransport.Serving():
		defer grpcTransport.Shutdown()
	case err = <-errChan:
		logger.Panic(err)
	case <-time.After(5 * time.Second):
		logger.Panic("Timed out waiting for all transport to serve")
	}
	close(errChan)

	logger.Info("TestyCool server is running")
	logger.Info("Press CTRL+C to gracefully shutdown server")

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	firstSig := <-signalChan // execution blocked here until signal is received
	logger.WithField("signal", firstSig).Info("Gracefully shutting down")

	// forcefully terminate after second signal before deferred functions are done
	go func() {
		secondSig := <-signalChan
		logger.WithField("signal", secondSig).Fatal("Forcefully shutting down")
	}()
}

func setAndCreateRuntimeDir(cfg *config.Config) error {
	// create directories
	err := os.MkdirAll(cfg.Runtime.DataDir, os.ModePerm)
	if err != nil {
		return fmt.Errorf("unable to create data directory: %w", err)
	}
	err = os.MkdirAll(cfg.Runtime.LogDir, os.ModePerm)
	if err != nil {
		return fmt.Errorf("unable to create log directory: %w", err)
	}
	return nil
}

func newServerLogger(cfg *config.Config) *logrus.Logger {
	logger := logrus.New()
	if cfg.Verbose {
		logger.SetLevel(logrus.DebugLevel)
	} else {
		logger.SetLevel(logrus.InfoLevel)
	}

	logger.SetFormatter(&logrus.TextFormatter{
		ForceQuote:    true,
		FullTimestamp: true,
		PadLevelText:  true,
	})
	return logger
}

func addLoggerFileHook(logger *logrus.Logger, cfg *config.Config) (*os.File, error) {
	logFileName := filepath.Join(cfg.Runtime.LogDir, "server.log")
	logFile, err := os.OpenFile(logFileName, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0o644)
	if err != nil {
		return nil, fmt.Errorf("unable to create server log file: %w", err)
	}

	logger.AddHook(lfshook.NewHook(
		logFile,
		&logrus.TextFormatter{
			DisableColors: true,
			ForceQuote:    true,
			FullTimestamp: true,
			PadLevelText:  false,
		},
	))

	return logFile, nil
}