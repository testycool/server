// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package grpc

import (
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"gitlab.com/testycool/server/internal/config"
)

var ErrAlreadyRunning = errors.New("grpc server is already running")

type Serving struct{}

type Transport interface {
	Serve() error
	Shutdown()
	Serving() <-chan Serving
}

type transport struct {
	logger   *logrus.Entry
	services *ServiceContainer
	config   *config.Config

	requestLogger  *logrus.Logger
	requestLogFile *os.File
	grpcServer     *grpc.Server
	isServing      bool
	servingChan    chan Serving
}

func (t *transport) Serve() error {
	t.logger.Debug("Starting server")

	if t.isServing {
		return ErrAlreadyRunning
	}

	// configure logger
	if t.config.GRPC.LogRequests {
		t.logger.Debug("Configuring request logger")
		err := t.configureRequestLogger()
		if err != nil {
			return fmt.Errorf("unable to configure grpc request logger: %w", err)
		}
	}

	// configure grpc server
	t.logger.Debug("Configuring server")
	if err := t.configureServer(); err != nil {
		return fmt.Errorf("unable to configure grpc server: %w", err)
	}

	// register grpc services
	t.logger.Debug("Registering services")
	if err := t.registerServices(); err != nil {
		return fmt.Errorf("unable to register grpc services: %w", err)
	}

	// setup grpc listener
	lisAddr := net.JoinHostPort(t.config.GRPC.Host, strconv.Itoa(t.config.GRPC.Port))
	t.logger.Debugf("Setting up net listener on %s", lisAddr)
	lis, err := net.Listen("tcp", lisAddr)
	if err != nil {
		return fmt.Errorf("failed to listen: %w", err)
	}

	t.logger.Infof("Server listening on %s:%d", t.config.GRPC.Host, t.config.GRPC.Port)
	if ip := net.ParseIP(t.config.GRPC.Host); ip != nil && ip.IsUnspecified() {
		t.logListenInterfaces()
	}

	t.isServing = true
	t.servingChan <- Serving{}

	err = t.grpcServer.Serve(lis)
	if err != nil {
		return fmt.Errorf("failed to serve: %w", err)
	}
	return nil
}

func (t *transport) Shutdown() {
	if !t.isServing {
		return
	}

	t.logger.Debug("Shutting down server")
	t.grpcServer.GracefulStop()

	t.isServing = false
}

func (t *transport) Serving() <-chan Serving {
	return t.servingChan
}

func (t *transport) logListenInterfaces() {
	addresses, err := net.InterfaceAddrs()
	if err != nil {
		return
	}
	for _, address := range addresses {
		ipNet, ok := address.(*net.IPNet)
		if ok && ipNet.IP.To4() != nil {
			t.logger.Infof(`Network found, listening at %s:%d`, ipNet.IP, t.config.GRPC.Port)
		}
	}
}

func NewTransport(logger *logrus.Entry, cfg *config.Config, svc *ServiceContainer) (Transport, error) {
	return &transport{
		logger:      logger,
		config:      cfg,
		services:    svc,
		servingChan: make(chan Serving, 1),
	}, nil
}
