// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package grpc

import (
	"errors"
	"reflect"

	"gitlab.com/testycool/server/internal/analytics"
	"gitlab.com/testycool/server/internal/answer"
	"gitlab.com/testycool/server/internal/attempt"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/choice"
	"gitlab.com/testycool/server/internal/exam"
	"gitlab.com/testycool/server/internal/jwt"
	"gitlab.com/testycool/server/internal/participant"
	"gitlab.com/testycool/server/internal/question"
)

var (
	ErrJWTServiceNotSet         = errors.New("jwt service not set")
	ErrAuthServiceNotSet        = errors.New("auth service not set")
	ErrExamServiceNotSet        = errors.New("exam service not set")
	ErrParticipantServiceNotSet = errors.New("participant service not set")
	ErrQuestionServiceNotSet    = errors.New("question service not set")
	ErrChoiceServiceNotSet      = errors.New("choice service not set")
	ErrAnswerServiceNotSet      = errors.New("answer service not set")
	ErrAttemptServiceNotSet     = errors.New("attempt service not set")
	ErrAnalyticsServiceNotSet   = errors.New("analytics service not set")
)

type ServiceContainer struct {
	JWTService         jwt.Service
	AuthService        auth.Service
	ExamService        exam.Service
	ParticipantService participant.Service
	QuestionService    question.Service
	ChoiceService      choice.Service
	AnswerService      answer.Service
	AttemptService     attempt.Service
	AnalyticsService   analytics.Service
}

func (s *ServiceContainer) Validate() error {
	if isNil(s.JWTService) {
		return ErrJWTServiceNotSet
	}
	if isNil(s.AuthService) {
		return ErrAuthServiceNotSet
	}
	if isNil(s.ExamService) {
		return ErrExamServiceNotSet
	}
	if isNil(s.ParticipantService) {
		return ErrParticipantServiceNotSet
	}
	if isNil(s.QuestionService) {
		return ErrQuestionServiceNotSet
	}
	if isNil(s.ChoiceService) {
		return ErrChoiceServiceNotSet
	}
	if isNil(s.AnswerService) {
		return ErrAnswerServiceNotSet
	}
	if isNil(s.AttemptService) {
		return ErrAttemptServiceNotSet
	}
	if isNil(s.AnalyticsService) {
		return ErrAnalyticsServiceNotSet
	}
	return nil
}

func isNil(i interface{}) bool {
	return i == nil || reflect.ValueOf(i).IsNil()
}