// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/answer"
	"gitlab.com/testycool/server/internal/attempt"
	"gitlab.com/testycool/server/internal/question"
)

type attemptServer struct {
	pb.UnimplementedAttemptServiceServer
	logger          *logrus.Entry
	attemptService  attempt.Service
	questionService question.Service
	answerService   answer.Service
}

func (s *attemptServer) GetAttempt(
	ctx context.Context,
	request *pb.GetAttemptRequest,
) (*pb.GetAttemptResponse, error) {
	var a attempt.Attempt
	var err error
	if id := request.GetId(); id != 0 {
		a, err = s.attemptService.GetByID(ctx, id)
	} else if participantId := request.GetParticipantId(); participantId != 0 {
		a, err = s.attemptService.GetByParticipantID(ctx, participantId)
	} else {
		return nil, status.Error(codes.InvalidArgument, "either id or participant id must be provided")
	}

	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetAttemptResponse{
		Attempt: s.attemptToProto(a),
	}, nil
}

func (s *attemptServer) ListAttempts(
	ctx context.Context,
	request *pb.ListAttemptsRequest,
) (*pb.ListAttemptsResponse, error) {
	size := request.GetSize()
	page := request.GetPage()

	listOpt := attempt.ListOptions{
		Size: size,
		Page: page,
	}

	if reqFilter := request.GetFilter(); reqFilter != nil {
		listOpt.Filter = attempt.Filter{
			ExamID: reqFilter.GetExamId(),
		}
	}

	listData, err := s.attemptService.List(ctx, listOpt)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var aa []*pb.Attempt
	for _, a := range listData.Attempts {
		aa = append(aa, s.attemptToProto(a))
	}

	currentPageSize := int32(len(aa))

	// if the actual requested size exceeds total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > listData.TotalSize {
		page = 1
	}

	return &pb.ListAttemptsResponse{
		Attempts:  aa,
		TotalSize: listData.TotalSize,
		Page:      page,
		Size:      currentPageSize,
	}, nil
}

func (s *attemptServer) CreateAttempt(
	ctx context.Context,
	request *pb.CreateAttemptRequest,
) (*pb.CreateAttemptResponse, error) {
	a := attempt.Attempt{
		ParticipantID: request.GetParticipantId(),
		ExamID:        request.GetExamId(),
		Finished:      false,
		Corrects:      0,
		Wrongs:        0,
		Unanswered:    0,
		CreatedAt:     request.GetCreatedAt().AsTime(),
		UpdatedAt:     request.GetCreatedAt().AsTime(),
	}

	a, err := s.attemptService.Save(ctx, a)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateAttemptResponse{
		Attempt: s.attemptToProto(a),
	}, nil
}

func (s *attemptServer) UpdateAttempt(ctx context.Context, request *pb.UpdateAttemptRequest) (*pb.UpdateAttemptResponse, error) {
	reqA := request.GetAttempt()
	if reqA == nil {
		return nil, status.Error(codes.InvalidArgument, "attempt data must be provided")
	}

	if reqA.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "attempt id must be provided")
	}

	a := attempt.Attempt{
		ID:            reqA.GetId(),
		ParticipantID: reqA.GetParticipantId(),
		ExamID:        reqA.GetExamId(),
		Finished:      reqA.GetFinished(),
		Corrects:      0,
		Wrongs:        0,
		Unanswered:    0,
		CreatedAt:     reqA.GetCreatedAt().AsTime(),
		UpdatedAt:     reqA.GetUpdatedAt().AsTime(),
	}

	questionOpts := question.ListOptions{
		Size: -1,
		Page: 1,
		Filter: question.Filter{
			ExamID: a.ExamID,
		},
	}

	answerOpts := answer.ListOptions{
		Size: -1,
		Page: 1,
		Filter: answer.Filter{
			ExamID:        a.ExamID,
			ParticipantID: a.ParticipantID,
		},
	}

	questions, qerr := s.questionService.List(ctx, questionOpts)
	if qerr != nil {
		return nil, errorToStatus(s.logger, qerr)
	}
	answers, aerr := s.answerService.List(ctx, answerOpts)
	if aerr != nil {
		return nil, errorToStatus(s.logger, aerr)
	}

	for i := 0; i < int(answers.TotalSize); i++ {
		if answers.Answers[i].Correct {
			a.Corrects += 1
		} else {
			a.Wrongs += 1
		}
	}
	a.Unanswered = questions.TotalSize - answers.TotalSize

	a, err := s.attemptService.Update(ctx, a)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateAttemptResponse{
		Attempt: s.attemptToProto(a),
	}, nil
}

func (s *attemptServer) DeleteAttempt(ctx context.Context, request *pb.DeleteAttemptRequest) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	err := s.attemptService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (s *attemptServer) attemptToProto(a attempt.Attempt) *pb.Attempt {
	pbAttempt := &pb.Attempt{
		Id:            a.ID,
		ParticipantId: a.ParticipantID,
		ExamId:        a.ExamID,
		Finished:      a.Finished,
		Corrects:      a.Corrects,
		Wrongs:        a.Wrongs,
		Unanswered:    a.Unanswered,
		CreatedAt:     timestamppb.New(a.CreatedAt),
		UpdatedAt:     timestamppb.New(a.UpdatedAt),
	}

	return pbAttempt
}

func NewAttemptServer(
	logger *logrus.Entry,
	attemptService attempt.Service,
	questionService question.Service,
	answerService answer.Service,
) pb.AttemptServiceServer {
	return &attemptServer{
		logger:          logger,
		attemptService:  attemptService,
		questionService: questionService,
		answerService:   answerService,
	}
}
