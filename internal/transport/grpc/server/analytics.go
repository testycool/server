// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/analytics"
)

type analyticsServer struct {
	pb.UnimplementedAnalyticsServiceServer
	logger           *logrus.Entry
	analyticsService analytics.Service
}

func (s *analyticsServer) CreateAnalytics(
	ctx context.Context,
	request *pb.CreateAnalyticsRequest,
) (*pb.CreateAnalyticsResponse, error) {
	p := analytics.Analytics{
		Message:       request.GetMessage(),
		CreatedAt:     request.GetCreatedAt().AsTime(),
		ParticipantID: request.GetParticipantId(),
		ExamID:        request.GetExamId(),
	}

	p, err := s.analyticsService.Save(ctx, p)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateAnalyticsResponse{
		Analytics: s.analyticsToProto(p),
	}, nil
}

func (s *analyticsServer) GetAnalytics(
	ctx context.Context,
	request *pb.GetAnalyticsRequest,
) (*pb.GetAnalyticsResponse, error) {
	var p analytics.Analytics
	var err error
	if id := request.GetId(); id != 0 {
		p, err = s.analyticsService.GetByID(ctx, id)
	} else {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetAnalyticsResponse{
		Analytics: s.analyticsToProto(p),
	}, nil
}

func (s *analyticsServer) ListAnalytics(
	ctx context.Context,
	request *pb.ListAnalyticsRequest,
) (*pb.ListAnalyticsResponse, error) {
	size := request.GetSize()
	page := request.GetPage()

	filter := analytics.ListFilter{
		ExamID: request.Filter.ExamId,
	}

	pp, totalSize, err := s.analyticsService.List(ctx, size, page, filter)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var analytics []*pb.Analytics
	for i := range pp {
		analytics = append(analytics, s.analyticsToProto(pp[i]))
	}

	currentPageSize := int32(len(analytics))

	// if the actual requested size exceed total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > totalSize {
		page = 1
	}

	return &pb.ListAnalyticsResponse{
		Analytics: analytics,
		TotalSize: totalSize,
		Page:      page,
		Size:      currentPageSize,
	}, nil
}

func (s *analyticsServer) UpdateAnalytics(
	ctx context.Context,
	request *pb.UpdateAnalyticsRequest,
) (*pb.UpdateAnalyticsResponse, error) {
	reqA := request.GetAnalytics()

	if reqA == nil {
		return nil, status.Error(codes.InvalidArgument, "analytics data is missing")
	}
	if reqA.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must not be zero")
	}

	p := analytics.Analytics{
		ID:            reqA.GetId(),
		Message:       reqA.GetMessage(),
		ParticipantID: reqA.GetParticipantId(),
		ExamID:        reqA.GetExamId(),
	}

	p, err := s.analyticsService.Update(ctx, p)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateAnalyticsResponse{
		Analytics: s.analyticsToProto(p),
	}, nil
}

func (s *analyticsServer) DeleteAnalytics(
	ctx context.Context,
	request *pb.DeleteAnalyticsRequest,
) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must not be zero")
	}

	err := s.analyticsService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (*analyticsServer) analyticsToProto(p analytics.Analytics) *pb.Analytics {
	return &pb.Analytics{
		Id:            p.ID,
		Message:       p.Message,
		ParticipantId: p.ParticipantID,
		ExamId:        p.ExamID,
		CreatedAt:     timestamppb.New(p.CreatedAt),
	}
}

func NewAnalyticsServer(logger *logrus.Entry, analyticsService analytics.Service) pb.AnalyticsServiceServer {
	return &analyticsServer{
		logger:           logger,
		analyticsService: analyticsService,
	}
}
