// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/auth"
)

type authServer struct {
	pb.UnimplementedAuthServiceServer
	logger      *logrus.Entry
	authService auth.Service
}

func (s *authServer) GetAdminToken(
	ctx context.Context,
	request *pb.GetAdminTokenRequest,
) (*pb.GetAdminTokenResponse, error) {
	token, err := s.authService.NewAdminToken(ctx, request.Passcode)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetAdminTokenResponse{
		AccessToken: token.AccessToken,
	}, nil
}

func (s *authServer) GetParticipantToken(
	ctx context.Context,
	request *pb.GetParticipantTokenRequest,
) (*pb.GetParticipantTokenResponse, error) {
	token, err := s.authService.NewParticipantToken(ctx, request.ExamPassword, request.ParticipantCode)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetParticipantTokenResponse{
		AccessToken: token.AccessToken,
	}, nil
}

func NewAuthServer(logger *logrus.Entry, authService auth.Service) pb.AuthServiceServer {
	return &authServer{
		logger:      logger,
		authService: authService,
	}
}
