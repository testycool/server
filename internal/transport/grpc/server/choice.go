// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/choice"
	"gitlab.com/testycool/server/internal/format"
)

type choiceServer struct {
	pb.UnimplementedChoiceServiceServer
	logger        *logrus.Entry
	choiceService choice.Service
}

func (s *choiceServer) GetChoice(
	ctx context.Context,
	request *pb.GetChoiceRequest,
) (*pb.GetChoiceResponse, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	c, err := s.choiceService.GetByID(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetChoiceResponse{
		Choice: s.choiceToProto(c),
	}, nil
}

func (s *choiceServer) ListChoices(
	ctx context.Context,
	request *pb.ListChoicesRequest,
) (*pb.ListChoicesResponse, error) {
	size := request.GetSize()
	page := request.GetPage()

	listOpt := choice.ListOptions{
		Size: size,
		Page: page,
	}

	if reqFilter := request.GetFilter(); reqFilter != nil {
		listOpt.Filter = choice.Filter{
			QuestionID: reqFilter.GetQuestionId(),
		}
	}

	listData, err := s.choiceService.List(ctx, listOpt)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var cc []*pb.Choice
	for _, q := range listData.Choices {
		cc = append(cc, s.choiceToProto(q))
	}

	currentPageSize := int32(len(cc))

	// if the actual requested size exceeds total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > listData.TotalSize {
		page = 1
	}

	return &pb.ListChoicesResponse{
		Choices:   cc,
		TotalSize: listData.TotalSize,
		Page:      page,
		Size:      currentPageSize,
	}, nil
}

func (s *choiceServer) CreateChoice(
	ctx context.Context,
	request *pb.CreateChoiceRequest,
) (*pb.CreateChoiceResponse, error) {
	c := choice.Choice{
		Correct:       request.GetIsCorrect(),
		ContentFormat: format.Text(request.GetFormat()),
		Content:       request.GetContent(),
		QuestionID:    request.GetQuestionId(),
	}

	c, err := s.choiceService.Save(ctx, c)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateChoiceResponse{
		Choice: s.choiceToProto(c),
	}, nil
}

func (s *choiceServer) UpdateChoice(ctx context.Context, request *pb.UpdateChoiceRequest) (*pb.UpdateChoiceResponse, error) {
	reqC := request.GetChoice()
	if reqC == nil {
		return nil, status.Error(codes.InvalidArgument, "choice data must be provided")
	}

	if reqC.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "choice id must be provided")
	}

	c := choice.Choice{
		ID:            reqC.GetId(),
		Correct:       reqC.GetIsCorrect(),
		ContentFormat: format.Text(reqC.GetFormat()),
		Content:       reqC.GetContent(),
		QuestionID:    reqC.GetQuestionId(),
	}

	c, err := s.choiceService.Update(ctx, c)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateChoiceResponse{
		Choice: s.choiceToProto(c),
	}, nil
}

func (s *choiceServer) DeleteChoice(ctx context.Context, request *pb.DeleteChoiceRequest) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	err := s.choiceService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (s *choiceServer) choiceToProto(c choice.Choice) *pb.Choice {
	return &pb.Choice{
		Id:         c.ID,
		QuestionId: c.QuestionID,
		IsCorrect:  c.Correct,
		Format:     pb.TextFormat(c.ContentFormat),
		Content:    c.Content,
	}
}

func NewChoiceServer(logger *logrus.Entry, choiceService choice.Service) pb.ChoiceServiceServer {
	return &choiceServer{
		logger:        logger,
		choiceService: choiceService,
	}
}
