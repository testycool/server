// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/format"
	"gitlab.com/testycool/server/internal/question"
)

type questionServer struct {
	pb.UnimplementedQuestionServiceServer
	logger          *logrus.Entry
	questionService question.Service
}

func (s *questionServer) GetQuestion(
	ctx context.Context,
	request *pb.GetQuestionRequest,
) (*pb.GetQuestionResponse, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	q, err := s.questionService.GetByID(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetQuestionResponse{
		Question: s.questionToProto(q),
	}, nil
}

func (s *questionServer) ListQuestions(
	ctx context.Context,
	request *pb.ListQuestionsRequest,
) (*pb.ListQuestionsResponse, error) {
	size := request.GetSize()
	page := request.GetPage()

	listOpt := question.ListOptions{
		Size: size,
		Page: page,
	}

	if reqFilter := request.GetFilter(); reqFilter != nil {
		listOpt.Filter = question.Filter{
			ExamID: reqFilter.GetExamId(),
		}
	}

	listData, err := s.questionService.List(ctx, listOpt)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var qq []*pb.Question
	for _, q := range listData.Questions {
		qq = append(qq, s.questionToProto(q))
	}

	currentPageSize := int32(len(qq))

	// if the actual requested size exceeds total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > listData.TotalSize {
		page = 1
	}

	return &pb.ListQuestionsResponse{
		Questions: qq,
		TotalSize: listData.TotalSize,
		Page:      page,
		Size:      currentPageSize,
	}, nil
}

func (s *questionServer) CreateQuestion(
	ctx context.Context,
	request *pb.CreateQuestionRequest,
) (*pb.CreateQuestionResponse, error) {
	q := question.Question{
		Type:          question.Type(request.GetType()),
		ContentFormat: format.Text(request.GetFormat()),
		Content:       request.GetContent(),
		ExamID:        request.GetExamId(),
	}

	q, err := s.questionService.Save(ctx, q)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateQuestionResponse{
		Question: s.questionToProto(q),
	}, nil
}

func (s *questionServer) UpdateQuestion(
	ctx context.Context,
	request *pb.UpdateQuestionRequest,
) (*pb.UpdateQuestionResponse, error) {
	reqQ := request.GetQuestion()
	if reqQ == nil {
		return nil, status.Error(codes.InvalidArgument, "question data must be provided")
	}

	if reqQ.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "question id must be provided")
	}

	q := question.Question{
		ID:            reqQ.GetId(),
		Type:          question.Type(reqQ.GetType()),
		ContentFormat: format.Text(reqQ.GetFormat()),
		Content:       reqQ.GetContent(),
		ExamID:        reqQ.GetExamId(),
	}

	q, err := s.questionService.Update(ctx, q)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateQuestionResponse{
		Question: s.questionToProto(q),
	}, nil
}

func (s *questionServer) DeleteQuestion(
	ctx context.Context,
	request *pb.DeleteQuestionRequest,
) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	err := s.questionService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (s *questionServer) questionToProto(q question.Question) *pb.Question {
	return &pb.Question{
		Id:      q.ID,
		ExamId:  q.ExamID,
		Type:    pb.Question_Type(q.Type),
		Format:  pb.TextFormat(q.ContentFormat),
		Content: q.Content,
	}
}

func NewQuestionServer(logger *logrus.Entry, questionService question.Service) pb.QuestionServiceServer {
	return &questionServer{
		logger:          logger,
		questionService: questionService,
	}
}
