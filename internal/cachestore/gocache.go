// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cachestore

import (
	"time"

	cache "github.com/patrickmn/go-cache"
)

type memoryCache struct {
	goCache *cache.Cache
}

func (s *memoryCache) Get(key string) (interface{}, bool) {
	return s.goCache.Get(key)
}

func (s *memoryCache) Set(key string, value interface{}) {
	s.goCache.Set(key, value, cache.NoExpiration)
}

func (s *memoryCache) SetWithTTL(key string, value interface{}, ttl time.Duration) {
	s.goCache.Set(key, value, ttl)
}

func (s *memoryCache) Delete(key string) {
	s.goCache.Delete(key)
}

func (s *memoryCache) Flush() {
	s.goCache.Flush()
}

func NewMemoryCache() Cache {
	return &memoryCache{
		goCache: cache.New(cache.NoExpiration, cache.NoExpiration),
	}
}