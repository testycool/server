// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"context"
)

type ContextKey string

const (
	IsAdminContextKey ContextKey = "is-admin"
	UserIDContextKey  ContextKey = "user-id"
	ExamIDContextKey  ContextKey = "exam-id"
)

func CheckAuthentication(ctx context.Context) error {
	_, ok := ctx.Value(UserIDContextKey).(int32)
	if !ok {
		return ErrAuthRequired
	}

	return nil
}

func CheckAdmin(ctx context.Context) error {
	err := CheckAuthentication(ctx)
	if err != nil {
		return err
	}

	uid, err := GetUserID(ctx)
	if err != nil {
		return err
	}

	if uid != -1 {
		return ErrAdminRequired
	}

	isAdmin, ok := ctx.Value(IsAdminContextKey).(bool)
	if !isAdmin || !ok {
		return ErrAdminRequired
	}

	return nil
}

func GetUserID(ctx context.Context) (int32, error) {
	id, ok := ctx.Value(UserIDContextKey).(int32)
	if ok {
		return id, nil
	}
	return 0, ErrAuthRequired
}
