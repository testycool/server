// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package webui

import (
	"embed"
	"html/template"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/config"
	"gitlab.com/testycool/server/internal/version"
)

//go:embed template/*
var templateFS embed.FS

type handlers struct {
	serverLogger *logrus.Logger
	config       *config.Config
	templates    *template.Template
	wsUpgrader   websocket.Upgrader
}

type gRPCData struct {
	Host       string
	Port       int
	Reflection bool
}

type wsData struct {
	Host     string
	Port     int
	LogsPath string
}

type indexData struct {
	Title   string
	Version string
	WS      wsData
	GRPC    gRPCData
}

func (h *handlers) Index(w http.ResponseWriter, r *http.Request) {
	data := indexData{
		Title:   "TestyCool WebUI",
		Version: version.Version,
		WS: wsData{
			Host:     "localhost",
			Port:     8080,
			LogsPath: "logs",
		},
		GRPC: gRPCData{
			Host:       h.config.GRPC.Host,
			Port:       h.config.GRPC.Port,
			Reflection: h.config.GRPC.EnableReflection,
		},
	}

	if err := h.templates.ExecuteTemplate(w, "index.gohtml", data); err != nil {
		h.serverLogger.WithError(err).Error("failed to execute template")
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}

func (h *handlers) Logs(w http.ResponseWriter, r *http.Request) {
	conn, err := h.wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		h.serverLogger.WithError(err).Error("failed to upgrade connection")
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer func() {
		_ = conn.Close()
	}()

	h.serverLogger.AddHook(NewWSHook(conn))

	for {
		time.Sleep(500 * time.Millisecond)
		err = conn.SetWriteDeadline(time.Now().Add(500 * time.Millisecond))
		if err != nil {
			h.serverLogger.WithError(err).Error("failed to set write deadline")
			return
		}
	}
}

type WebUI struct {
	logger *logrus.Entry
	config *config.Config
}

func (w *WebUI) Serve() error {
	t, err := template.ParseFS(templateFS, "template/*.gohtml")
	if err != nil {
		return err
	}

	handlers := &handlers{
		templates:    t,
		config:       w.config,
		serverLogger: w.logger.Logger,
		wsUpgrader:   websocket.Upgrader{},
	}

	http.HandleFunc("/", handlers.Index)
	http.HandleFunc("/logs", handlers.Logs)

	if err = http.ListenAndServe(":8080", nil); err != nil {
		return err
	}

	return nil
}

func NewWebUI(logger *logrus.Entry, cfg *config.Config) (*WebUI, error) {
	return &WebUI{
		logger: logger,
		config: cfg,
	}, nil
}
