// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package webui

import (
	"sync"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type WSHook struct {
	ws        *websocket.Conn
	formatter *logrus.TextFormatter
	lock      *sync.Mutex
}

func (hook *WSHook) Fire(entry *logrus.Entry) error {
	hook.lock.Lock()
	defer hook.lock.Unlock()

	bs, err := hook.formatter.Format(entry)
	if err != nil {
		_ = hook.ws.Close()
		return err
	}

	err = hook.ws.WriteMessage(websocket.TextMessage, bs)
	if err != nil {
		_ = hook.ws.Close()
		return err
	}

	return nil
}

func (hook *WSHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func NewWSHook(ws *websocket.Conn) *WSHook {
	return &WSHook{
		ws: ws,
		formatter: &logrus.TextFormatter{
			DisableColors: true,
			ForceQuote:    true,
		},
		lock: new(sync.Mutex),
	}
}
