// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package sqlite

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"zombiezen.com/go/sqlite/sqlitemigration"
	"zombiezen.com/go/sqlite/sqlitex"
)

type ByNumericalFileName []fs.DirEntry

func (nf ByNumericalFileName) Len() int {
	return len(nf)
}

func (nf ByNumericalFileName) Swap(i, j int) {
	nf[i], nf[j] = nf[j], nf[i]
}

func (nf ByNumericalFileName) Less(i, j int) bool {
	// Use path names
	pathA := nf[i].Name()
	pathB := nf[j].Name()

	// Grab integer value of each filename by parsing the string and slicing off
	// the extension
	a, err1 := strconv.ParseInt(pathA[0:strings.LastIndex(pathA, ".")], 10, 64)
	b, err2 := strconv.ParseInt(pathB[0:strings.LastIndex(pathB, ".")], 10, 64)

	// If any were not numbers sort lexicographically
	if err1 != nil || err2 != nil {
		return pathA < pathB
	}

	// Which integer is smaller?
	return a < b
}

func Migrate(dbPool *sqlitex.Pool, migrationFS embed.FS) error {
	migrationCtx := context.Background()
	schema := sqlitemigration.Schema{
		AppID: 0xC0FF33,
	}

	const migrationsDir = "migrations"

	dirEntries, err := migrationFS.ReadDir(migrationsDir)
	if err != nil {
		return fmt.Errorf("unable to read migrations directory: %w", err)
	}
	sort.Sort(ByNumericalFileName(dirEntries))

	for _, e := range dirEntries {
		if e.IsDir() || filepath.Ext(e.Name()) != ".sql" {
			continue
		}
		migration, err := migrationFS.ReadFile(migrationsDir + "/" + e.Name()) //nolint:govet // shadow err
		if err != nil {
			return fmt.Errorf("unable to read migration file: %w", err)
		}
		schema.Migrations = append(schema.Migrations, string(migration))
	}

	conn := dbPool.Get(migrationCtx)
	defer dbPool.Put(conn)

	err = sqlitemigration.Migrate(migrationCtx, conn, schema)
	if err != nil {
		return fmt.Errorf("sqlite migration failed: %w", err)
	}

	return nil
}
