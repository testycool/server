// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datastore

import (
	"errors"
	"fmt"
)

var (
	// ErrNoRows is returned when no rows are returned from a query.
	ErrNoRows = errors.New("no rows returned")
	// ErrNotUnique is returned when a unique constraint is violated.
	ErrNotUnique = errors.New("unique constraint not satisfied")
)

type Error struct {
	StoreName    string
	Message      string
	wrappedError error
}

func (e *Error) Error() string {
	if e.wrappedError != nil {
		return fmt.Sprintf("store error [StoreName = %s]: %s: %s", e.StoreName, e.Message, e.wrappedError.Error())
	}
	return fmt.Sprintf("store error [StoreName = %s]: %s", e.StoreName, e.Message)
}

func (e *Error) Unwrap() error {
	return e.wrappedError
}

func NewError(storeName string, message string, err error) *Error {
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}

func SaveError(storeName string, message string, err error) *Error {
	message = fmt.Sprintf("save: %s", message)
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}

func GetByIDError(storeName string, message string, id int32, err error) *Error {
	message = fmt.Sprintf("get by id [id = %d]: %s", id, message)
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}

func ListError(storeName string, message string, offset int32, limit int32, filter interface{}, err error) *Error {
	message = fmt.Sprintf("list [offset = %d, limit = %d, filter = %#v]: %s", offset, limit, filter, message)
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}

func TotalSizeError(storeName string, message string, filter interface{}, err error) *Error {
	message = fmt.Sprintf("list [filter = %#v]: %s", filter, message)
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}

func UpdateError(storeName string, message string, id int32, err error) *Error {
	message = fmt.Sprintf("update [id = %d]: %s", id, message)
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}

func DeleteError(storeName string, message string, id int32, err error) *Error {
	message = fmt.Sprintf("delete [id = %d]: %s", id, message)
	return &Error{
		StoreName:    storeName,
		Message:      message,
		wrappedError: err,
	}
}
