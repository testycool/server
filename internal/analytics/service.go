// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package analytics

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/cachestore"
	"gitlab.com/testycool/server/internal/datastore"
)

type ListFilter struct {
	ExamID *int32
}

// Service describes the interface of a service that contains business logic for analytics.
type Service interface {
	// Save saves a analytics to the store.
	Save(ctx context.Context, a Analytics) (Analytics, error)
	// GetByID returns a analytics by its ID.
	GetByID(ctx context.Context, id int32) (Analytics, error)
	// List returns a list of analytics paginated by the given parameters.
	// size is the number of analytics to return. page is the page number.
	// Returns a list of analytics and the total number of analytics.
	List(ctx context.Context, size int32, page int32, filter ListFilter) ([]Analytics, int32, error)
	// Update updates a analytics in the store.
	Update(ctx context.Context, a Analytics) (Analytics, error)
	// Delete deletes a analytics from the store by its ID.
	Delete(ctx context.Context, id int32) error
}

// service is a concrete implementation of the Service interface.
type service struct {
	analyticsStore Store
	logger         *logrus.Entry
	singleCache    cachestore.Cache
	listCache      cachestore.Cache
}

func (s *service) Save(ctx context.Context, a Analytics) (Analytics, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return Analytics{}, apperror.Unauthenticated("authentication required to create new analytics", err)
	}

	if uid != auth.AdminUserID {
		a.ParticipantID = uid
	}

	err = a.ValidateNonGenerated()
	if err != nil {
		return Analytics{}, apperror.InvalidData("new analytics data is invalid: "+err.Error(), err)
	}

	a, err = s.analyticsStore.Save(ctx, a)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Analytics{}, apperror.Internal("unable get newly created analytics", err)
		}
		return Analytics{}, apperror.Internal("unable to save analytics", err)
	}

	return a, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Analytics, error) {
	// first, check if the request comes from an admin
	err := auth.CheckAdmin(ctx)
	if err != nil {
		// if not admin, check if user is analytics by getting its user ID.
		// analytics can only get their own data.
		id, err = auth.GetUserID(ctx)
		if err != nil {
			// request is not from admin nor analytics, return error.
			return Analytics{}, apperror.Unauthenticated("authentication required to get analytics data", err)
		}
	}

	a, err := s.analyticsStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("analytics with id = %d not found", id)
			return Analytics{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to get analytics with id = %d", id)
		return Analytics{}, apperror.Internal(msg, err)
	}

	return a, nil
}

func (s *service) List(ctx context.Context, size int32, page int32, filter ListFilter) ([]Analytics, int32, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return nil, 0, apperror.Unauthorized("admin access is required to list analytics", err)
	}

	if size < -1 {
		msg := fmt.Sprintf("invalid page size (%d < -1)", size)
		return nil, 0, apperror.OutOfRange(msg, nil)
	}

	if page < 0 {
		msg := fmt.Sprintf("invalid page number (%d < 1)", page)
		return nil, 0, apperror.OutOfRange(msg, nil)
	}

	if filter.ExamID != nil {
		analytics, err := s.analyticsStore.ListAllByExamID(ctx, *filter.ExamID)
		if err != nil {
			msg := fmt.Sprintf("unable to list analytics with exam id = %d", *filter.ExamID)
			return nil, 0, apperror.Internal(msg, err)
		}
		return analytics, int32(len(analytics)), nil
	}

	if size == -1 {
		analytics, err := s.analyticsStore.ListAll(ctx)
		if err != nil {
			return nil, 0, apperror.Internal("unable to list analytics", err)
		}
		return analytics, int32(len(analytics)), nil
	}

	totalSize, err := s.analyticsStore.GetTotalSize(ctx)
	if err != nil {
		return nil, 0, apperror.Internal("unable to get total size while getting exam list", err)
	}

	offset := size * (page - 1)
	analytics, err := s.analyticsStore.ListPaginated(ctx, offset, size)
	if err != nil {
		return nil, 0, apperror.Internal("unable to list analytics", err)
	}

	if analytics == nil {
		analytics = make([]Analytics, 0)
	}

	return analytics, totalSize, nil
}

func (s *service) Update(ctx context.Context, a Analytics) (Analytics, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Analytics{}, apperror.Unauthorized("admin access is required to update analytics", err)
	}

	if a.ID == 0 {
		return Analytics{}, apperror.InvalidData("id is required to update analytics", nil)
	}

	err = a.ValidateNonGenerated()
	if err != nil {
		msg := fmt.Sprintf("update data for analytics with id = %d is invalid: %v", a.ID, err)
		return Analytics{}, apperror.InvalidData(msg, err)
	}

	a, err = s.analyticsStore.Update(ctx, a)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("analytics with id = %d not found", a.ID)
			return Analytics{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to update analytics with id = %d", a.ID)
		return Analytics{}, apperror.Internal(msg, err)
	}

	return a, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete analytics", err)
	}

	err = s.analyticsStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete analytics with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

func NewService(logger *logrus.Entry, analyticsStore Store) (Service, error) {
	return &service{
		analyticsStore: analyticsStore,
		logger:         logger,
		singleCache:    cachestore.NewNoopCache(),
		listCache:      cachestore.NewNoopCache(),
	}, nil
}
