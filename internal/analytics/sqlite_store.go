// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package analytics

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
)

type sqliteStore struct {
	logger *logrus.Logger
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, a Analytics) (Analytics, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into analytics (message, created_at, exam_id, participant_id)" +
		" values($message, $created_at, $exam_id, $participant_id)" +
		" returning id, message, created_at, exam_id, participant_id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Analytics{}, fmt.Errorf("analytics sqliteStore: unable to save analytics: %w", err)
	}
	stmt.SetText("$message", a.Message)
	stmt.SetInt64("$created_at", a.CreatedAt.Unix())
	stmt.SetInt64("$exam_id", int64(a.ExamID))
	stmt.SetInt64("$participant_id", int64(a.ParticipantID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Analytics{}, fmt.Errorf("analytics sqliteStore: unable to save analytics: %w", err)
	}
	if !hasRow {
		return Analytics{}, fmt.Errorf(
			"analytics sqliteStore: unable to get newly saved analytics: %w",
			datastore.ErrNoRows,
		)
	}

	a = s.analyticsFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Analytics{}, fmt.Errorf("analytics sqliteStore: unable to finalize save: %w", err)
	}

	return a, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Analytics, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, message, created_at, exam_id, participant_id from analytics where id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Analytics{}, fmt.Errorf("analytics sqliteStore: unable to get analytics with id %d: %w", id, err)
	}
	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Analytics{}, fmt.Errorf("analytics sqliteStore: unable to get analytics with id %d: %w", id, err)
	}
	if !hasRow {
		return Analytics{}, fmt.Errorf(
			"analytics sqliteStore: unable to get analytics with id %d: %w",
			id,
			datastore.ErrNoRows,
		)
	}

	a := s.analyticsFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Analytics{}, fmt.Errorf("analytics sqliteStore: unable to get analytics with id %d: %w", id, err)
	}

	return a, nil
}

func (s *sqliteStore) ListAll(ctx context.Context) ([]Analytics, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, message, created_at, exam_id, participant_id from analytics;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf("analytics sqliteStore: unable to list all analytics: %w", err)
	}

	return s.execListStmt(stmt)
}

func (s *sqliteStore) ListAllByExamID(ctx context.Context, examID int32) ([]Analytics, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, message, created_at, exam_id, participant_id from analytics where exam_id = $exam_id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf(
			"analytics sqliteStore: unable to list analytics with exam_id = %d: %w",
			examID,
			err,
		)
	}
	stmt.SetInt64("$exam_id", int64(examID))

	return s.execListStmt(stmt)
}

func (s *sqliteStore) ListPaginated(ctx context.Context, offset int32, limit int32) ([]Analytics, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, message, created_at, exam_id, participant_id from analytics limit $limit offset $offset;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf(
			"analytics sqliteStore: unable to prepare list analytics query with offset = %d and limit = %d: %w",
			offset,
			limit,
			err,
		)
	}
	stmt.SetInt64("$limit", int64(limit))
	stmt.SetInt64("$offset", int64(offset))

	return s.execListStmt(stmt)
}

func (s *sqliteStore) GetTotalSize(ctx context.Context) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select count(*) as total_size from analytics;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return 0, fmt.Errorf("analytics sqliteStore: unable to prepare total size query: %w", err)
	}

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, fmt.Errorf("analytics sqliteStore: unable to execute get total size statement: %w", err)
	}
	if !hasRow {
		return 0, fmt.Errorf(
			"analytics sqliteStore: total size query returned no rows: %w",
			datastore.ErrNoRows,
		)
	}

	totalSize := stmt.GetInt64("total_size")

	err = stmt.Finalize()
	if err != nil {
		return 0, fmt.Errorf("analytics sqliteStore: unable to finalize get total size statement: %w", err)
	}

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, a Analytics) (Analytics, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update analytics set " +
		" message = $message, created_at = $created_at, exam_id = $exam_id, participant_id = $participant_id" +
		" where id = $id returning id, message, created_at, exam_id, participant_id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Analytics{}, fmt.Errorf(
			"analytics sqliteStore: unable to update analytics with id %d: %w",
			a.ID,
			err,
		)
	}

	stmt.SetText("$message", a.Message)
	stmt.SetInt64("$created_at", a.CreatedAt.Unix())
	stmt.SetInt64("$exam_id", int64(a.ExamID))
	stmt.SetInt64("$participant_id", int64(a.ParticipantID))
	stmt.SetInt64("$id", int64(a.ID))

	hasRow, err := stmt.Step()
	if err != nil {
		if message := sqlite.ErrCode(err); message == sqlite.ResultConstraintUnique {
			return Analytics{}, datastore.ErrNotUnique
		}
		return Analytics{}, fmt.Errorf(
			"analytics sqliteStore: unable to update analytics with id %d: %w",
			a.ID,
			err,
		)
	}
	if !hasRow {
		return Analytics{}, fmt.Errorf(
			"analytics sqliteStore: unable to get updated analytics with id %d: %w",
			a.ID,
			datastore.ErrNoRows,
		)
	}

	a = s.analyticsFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Analytics{}, fmt.Errorf(
			"analytics sqliteStore: unable to finalize update analytics for id %d: %w",
			a.ID,
			err,
		)
	}

	return a, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from analytics where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return fmt.Errorf("analytics sqliteStore: unable to delete analytics with id %d: %w", id, err)
	}

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return fmt.Errorf("analytics sqliteStore: unable to delete analytics with id %d: %w", id, err)
	}

	err = stmt.Finalize()
	if err != nil {
		return fmt.Errorf(
			"analytics sqliteStore: unable to finalize delete for analytics with id %d: %w",
			id,
			err,
		)
	}

	return nil
}

func (s *sqliteStore) analyticsFromStmt(stmt *sqlite.Stmt) Analytics {
	return Analytics{
		ID:            int32(stmt.GetInt64("id")),
		Message:       stmt.GetText("message"),
		CreatedAt:     time.Unix(stmt.GetInt64("created_at"), 0),
		ExamID:        int32(stmt.GetInt64("exam_id")),
		ParticipantID: int32(stmt.GetInt64("participant_id")),
	}
}

func (s *sqliteStore) execListStmt(stmt *sqlite.Stmt) ([]Analytics, error) {
	var analytics []Analytics
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			return nil, fmt.Errorf("analytics sqliteStore: unable to execute list analytics statement: %w", err)
		}
		if !hasRow {
			break
		}

		analytics = append(analytics, s.analyticsFromStmt(stmt))
	}

	err := stmt.Finalize()
	if err != nil {
		return nil, fmt.Errorf("analytics sqliteStore: unable to finalize list analytics statement: %w", err)
	}

	return analytics, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	return &sqliteStore{
		logger: logger,
		dbPool: dbPool,
	}, nil
}
