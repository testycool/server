// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package participant

import (
	"errors"
)

var (
	// ErrInvalidID is returned when the participant ID is invalid. ID should be greater than 0.
	ErrInvalidID = errors.New("invalid id")
	// ErrInvalidCode is returned when the participant code is invalid. Code should not be an empty string.
	ErrInvalidCode = errors.New("invalid code")
	// ErrInvalidName is returned when the participant name is invalid. Name should not be an empty string.
	ErrInvalidName = errors.New("invalid name")
	// ErrInvalidExamID is returned when the participants exam ID is invalid. Exam ID should be greater than 0.
	ErrInvalidExamID = errors.New("invalid exam id")
)

// Participant is a participant in an exam.
type Participant struct {
	ID     int32
	Code   string
	Name   string
	ExamID int32
}

// ValidateNonGenerated validates the participant data without validating auto-generated values.
func (p *Participant) ValidateNonGenerated() error {
	if p.ID < 0 {
		return ErrInvalidID
	}
	if p.Name == "" {
		return ErrInvalidName
	}
	if p.ExamID <= 0 {
		return ErrInvalidExamID
	}
	return nil
}

// Validate validates the participant data.
func (p *Participant) Validate() error {
	if p.ID <= 0 {
		return ErrInvalidID
	}
	if p.Code == "" {
		return ErrInvalidCode
	}
	if p.Name == "" {
		return ErrInvalidName
	}
	if p.ExamID <= 0 {
		return ErrInvalidExamID
	}
	return nil
}
