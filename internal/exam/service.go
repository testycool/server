// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package exam

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/datastore"
)

type Service interface {
	Save(ctx context.Context, exam Exam) (Exam, error)
	GetByID(ctx context.Context, id int32) (Exam, error)
	GetByPassword(ctx context.Context, password string) (Exam, error)
	List(ctx context.Context, size int32, page int32) ([]Exam, int32, error)
	Update(ctx context.Context, exam Exam) (Exam, error)
	Delete(ctx context.Context, id int32) error
}

type service struct {
	logger    *logrus.Logger
	examStore Store
}

func (s *service) Save(ctx context.Context, exam Exam) (Exam, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Exam{}, apperror.Unauthorized("admin access is required to create new exam", err)
	}
	err = exam.Validate()
	if err != nil {
		return Exam{}, apperror.InvalidData("new exam data is invalid: "+err.Error(), err)
	}

	exam, err = s.examStore.Save(ctx, exam)
	if err != nil {
		if errors.Is(err, datastore.ErrNotUnique) {
			// the only unique constraint in exam is on password
			return Exam{}, apperror.Duplicate("exam with the same password already exists", err)
		}
		if errors.Is(err, datastore.ErrNoRows) {
			return Exam{}, apperror.Internal("unable to get newly created exam", err)
		}
		return Exam{}, apperror.Internal("unable to save exam", err)
	}

	return exam, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Exam, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Exam{}, apperror.Unauthenticated("authentication required to get exam data", err)
	}

	exam, err := s.examStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("exam with id = %d does not exist", id)
			return Exam{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to get data for exam with id = %d", id)
		return Exam{}, apperror.Internal(msg, err)
	}

	// TODO: validate user exam id with requested exam id

	return exam, nil
}

func (s *service) GetByPassword(ctx context.Context, password string) (Exam, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Exam{}, apperror.Unauthenticated("authentication required to get exam data", err)
	}

	exam, err := s.examStore.GetByPassword(ctx, password)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("exam with password = '%s' does not exist", password)
			return Exam{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to get data for exam with password = '%s'", password)
		return Exam{}, apperror.Internal(msg, err)
	}

	// TODO: validate user exam id with requested exam id

	return exam, nil
}

func (s *service) List(ctx context.Context, size int32, page int32) ([]Exam, int32, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return nil, 0, apperror.Unauthorized("admin access is required to list exams", err)
	}

	if size < -1 {
		msg := fmt.Sprintf("invalid page size (%d < -1)", size)
		return nil, 0, apperror.OutOfRange(msg, nil)
	}

	if page < 1 {
		msg := fmt.Sprintf("invalid page number (%d < 1)", page)
		return nil, 0, apperror.OutOfRange(msg, nil)
	}

	var exams []Exam

	if size == -1 {
		exams, err = s.examStore.ListAll(ctx)
		if err != nil {
			return nil, 0, apperror.Internal("unable to list exams", err)
		}
		if len(exams) == 0 {
			return make([]Exam, 0), 0, nil
		}
		return exams, int32(len(exams)), nil
	}

	totalSize, err := s.examStore.GetTotalSize(ctx)
	if err != nil {
		return nil, 0, apperror.Internal("unable to get total size while getting exam list", err)
	}

	offset := size * (page - 1)
	exams, err = s.examStore.ListPaginated(ctx, offset, size)
	if err != nil {
		return nil, 0, apperror.Internal("unable to list exams", err)
	}

	if len(exams) == 0 {
		return make([]Exam, 0), totalSize, nil
	}

	return exams, totalSize, nil
}

func (s *service) Update(ctx context.Context, exam Exam) (Exam, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Exam{}, apperror.Unauthorized("admin access is required to update exam", err)
	}

	if exam.ID == 0 {
		return Exam{}, apperror.InvalidData("id is required to update exam", err)
	}
	err = exam.Validate()
	if err != nil {
		msg := fmt.Sprintf("update data for exam with id = %d is invalid: %v", exam.ID, err)
		return Exam{}, apperror.InvalidData(msg, err)
	}

	exam, err = s.examStore.Update(ctx, exam)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("exam with id = %d does not exist", exam.ID)
			return Exam{}, apperror.NotFound(msg, err)
		}
		if errors.Is(err, datastore.ErrNotUnique) {
			// the only unique constraint in exam is on password
			return Exam{}, apperror.Duplicate("exam with the same password already exists", err)
		}
		msg := fmt.Sprintf("unable to update exam with id = %d", exam.ID)
		return Exam{}, apperror.Internal(msg, err)
	}

	return exam, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete exam", err)
	}

	err = s.examStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete exam with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

func NewService(logger *logrus.Logger, examStore Store) (Service, error) {
	return &service{
		logger:    logger,
		examStore: examStore,
	}, nil
}
