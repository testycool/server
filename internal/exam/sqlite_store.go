// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package exam

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
)

type sqliteStore struct {
	logger *logrus.Logger
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, exam Exam) (Exam, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into exams (password, title, time_limit, start_at)" +
		" values ($password, $title, $time_limit, $start_at)" +
		" returning id, password, title, status, time_limit, start_at;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to save exam: %w", err)
	}
	stmt.SetText("$password", exam.Password)
	stmt.SetText("$title", exam.Title)
	stmt.SetInt64("$time_limit", int64(exam.TimeLimit.Seconds()))
	stmt.SetInt64("$start_at", exam.StartAt.Unix())

	hasRow, err := stmt.Step()
	if err != nil {
		if code := sqlite.ErrCode(err); code == sqlite.ResultConstraintUnique {
			return Exam{}, datastore.ErrNotUnique
		}
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to save exam: %w", err)
	}
	if !hasRow {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get newly saved exam: %w",
			datastore.ErrNoRows,
		)
	}

	exam = s.examFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to finalize save: %w", err)
	}

	return exam, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Exam, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, password, title, status, time_limit, start_at from exams" +
		" where id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to get exam with id %d: %w", id, err)
	}
	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to get exam with id %d: %w", id, err)
	}
	if !hasRow {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get exam with id %d: %w",
			id,
			datastore.ErrNoRows,
		)
	}

	exam := s.examFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to get exam with id %d: %w", id, err)
	}

	return exam, nil
}

func (s *sqliteStore) GetByPassword(ctx context.Context, password string) (Exam, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, password, title, status, time_limit, start_at from exams" +
		" where password = $password limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get exam with password '%s': %w",
			password,
			err,
		)
	}
	stmt.SetText("$password", password)

	hasRow, err := stmt.Step()
	if err != nil {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get exam with password '%s': %w",
			password,
			err,
		)
	}
	if !hasRow {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get exam with id '%s': %w",
			password,
			datastore.ErrNoRows,
		)
	}

	exam := s.examFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get exam with id '%s': %w",
			password,
			datastore.ErrNoRows,
		)
	}

	return exam, nil
}

func (s *sqliteStore) ListAll(ctx context.Context) ([]Exam, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, password, title, status, time_limit, start_at from exams;"
	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf("exam sqliteStore: unable to prepare list all query: %w", err)
	}

	return s.execListStmt(stmt)
}

func (s *sqliteStore) ListPaginated(ctx context.Context, offset int32, limit int32) ([]Exam, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, password, title, status, time_limit, start_at" +
		" from exams" +
		" limit $limit offset $offset ;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf(
			"exam sqliteStore: unable to prepare list exams query with offset = %d and limit = %d: %w",
			offset,
			limit,
			err,
		)
	}
	stmt.SetInt64("$limit", int64(limit))
	stmt.SetInt64("$offset", int64(offset))

	return s.execListStmt(stmt)
}

func (s *sqliteStore) GetTotalSize(ctx context.Context) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select count(*) as total_size from exams;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return 0, fmt.Errorf("exam sqliteStore: unable to prepare list all query: %w", err)
	}

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, fmt.Errorf("exam sqliteStore: unable to execute get total size statement: %w", err)
	}
	if !hasRow {
		return 0, fmt.Errorf(
			"exam sqliteStore: total size query returned no rows: %w",
			datastore.ErrNoRows,
		)
	}

	totalSize := stmt.GetInt64("total_size")

	err = stmt.Finalize()
	if err != nil {
		return 0, fmt.Errorf("exam sqliteStore: unable to finalize get total size statement: %w", err)
	}

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, exam Exam) (Exam, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update exams set" +
		" password = $password, title = $title, time_limit = $time_limit, start_at = $start_at, status = $status" +
		" where id = $id returning id, password, title, status, time_limit, start_at;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to update exam with id %d: %w", exam.ID, err)
	}

	stmt.SetText("$password", exam.Password)
	stmt.SetText("$title", exam.Title)
	stmt.SetInt64("$time_limit", int64(exam.TimeLimit.Seconds()))
	stmt.SetInt64("$start_at", exam.StartAt.Unix())
	stmt.SetInt64("$status", int64(exam.Status))
	stmt.SetInt64("$id", int64(exam.ID))

	hasRow, err := stmt.Step()
	if err != nil {
		if code := sqlite.ErrCode(err); code == sqlite.ResultConstraintUnique {
			return Exam{}, datastore.ErrNotUnique
		}
		return Exam{}, fmt.Errorf("exam sqliteStore: unable to update exam with id %d: %w", exam.ID, err)
	}
	if !hasRow {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to get updated exam with id %d: %w",
			exam.ID,
			datastore.ErrNoRows,
		)
	}

	exam = s.examFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Exam{}, fmt.Errorf(
			"exam sqliteStore: unable to finalize update for id %d: %w",
			exam.ID,
			err,
		)
	}

	return exam, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from exams where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return fmt.Errorf("exam sqliteStore: unable to delete exam with id %d: %w", id, err)
	}

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return fmt.Errorf("exam sqliteStore: unable to delete exam with id %d: %w", id, err)
	}

	err = stmt.Finalize()
	if err != nil {
		return fmt.Errorf("exam sqliteStore: unable to finalize delete for exam with id %d: %w", id, err)
	}

	return nil
}

func (s *sqliteStore) execListStmt(stmt *sqlite.Stmt) ([]Exam, error) {
	var exams []Exam
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			return nil, fmt.Errorf("exam sqliteStore: unable to execute list exams statement: %w", err)
		}
		if !hasRow {
			break
		}
		exams = append(exams, s.examFromStmt(stmt))
	}

	err := stmt.Finalize()
	if err != nil {
		return nil, fmt.Errorf("exam sqliteStore: unable to finalize list exams statement: %w", err)
	}

	return exams, nil
}

func (*sqliteStore) examFromStmt(stmt *sqlite.Stmt) Exam {
	return Exam{
		ID:        int32(stmt.GetInt64("id")),
		Password:  stmt.GetText("password"),
		Title:     stmt.GetText("title"),
		TimeLimit: time.Duration(stmt.GetInt64("time_limit")) * time.Second,
		StartAt:   time.Unix(stmt.GetInt64("start_at"), 0),
		Status:    Status(stmt.GetInt64("status")),
	}
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	return &sqliteStore{
		logger: logger,
		dbPool: dbPool,
	}, nil
}
