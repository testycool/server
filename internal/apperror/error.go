// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package apperror

import (
	"errors"

	"google.golang.org/grpc/codes"
)

// Error is an error with a human-readable message, wrapped error, and transport codes.
type Error struct {
	httpCode   int
	grpcCode   codes.Code
	message    string
	wrappedErr error
}

// Error implements the error interface.
func (e *Error) Error() string {
	if e != nil {
		if e.wrappedErr != nil {
			return e.message + ": " + e.wrappedErr.Error()
		}
		return e.message
	}
	return ""
}

// Unwrap implements the errors.Wrapper interface.
func (e *Error) Unwrap() error {
	if e != nil {
		return e.wrappedErr
	}
	return nil
}

// Message returns the human-readable message associated with the error
// along with human-readable message of the wrapped error (if it implements Error).
func (e *Error) Message() string {
	if e != nil {
		// if wrappedErr is of type Error, get its message
		var we *Error
		if errors.As(e.wrappedErr, &we) {
			return e.message + ": " + we.Message()
		}
		return e.message
	}
	return ""
}

// HttpCode returns the HTTP status code associated with the error.
func (e *Error) HttpCode() int {
	if e != nil {
		return e.httpCode
	}
	return 0
}

// GrpcCode returns the gRPC error code associated with the error.
func (e *Error) GrpcCode() codes.Code {
	if e != nil {
		return e.grpcCode
	}
	return 0
}

// New returns a new error with the specified human-readable message, HTTP status code, and gRPC error code.
func New(message string, err error, httpCode int, grpcCode codes.Code) *Error {
	return &Error{
		httpCode:   httpCode,
		grpcCode:   grpcCode,
		message:    message,
		wrappedErr: err,
	}
}
