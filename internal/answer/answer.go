// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package answer

import (
	"errors"
	"fmt"

	"gitlab.com/testycool/server/internal/format"
)

var (
	ErrInvalidID           = errors.New("invalid answer id")
	ErrInvalidType         = errors.New("invalid answer type")
	ErrInvalidEssayFormat  = errors.New("invalid essay content format")
	ErrInvalidEssayContent = errors.New("invalid essay content")
	ErrInvalidChoiceID     = errors.New("invalid choice id")
)

type Answer struct {
	ID            int32 `json:"id,omitempty"`
	Correct       bool  `json:"correct"`
	ParticipantID int32 `json:"participant_id,omitempty"`
	QuestionID    int32 `json:"question_id,omitempty"`
	// Types that are valid to be assigned to Content:
	//	ContentChoiceID
	//	ContentEssay
	Content isAnswerContent `json:"content,omitempty"`
}

func (a *Answer) Validate() error {
	if a.ID < 0 {
		return ErrInvalidID
	}

	if a.ParticipantID < 0 {
		return ErrInvalidID
	}

	if a.QuestionID < 0 {
		return ErrInvalidID
	}

	switch c := a.Content.(type) {
	case ContentChoiceID, ContentEssay:
		return c.Validate()
	default:
		return ErrInvalidChoiceID
	}
}

func (a *Answer) ValidateNew() error {
	if a.ParticipantID < 0 {
		return ErrInvalidID
	}

	if a.QuestionID < 0 {
		return ErrInvalidID
	}

	switch c := a.Content.(type) {
	case ContentChoiceID, ContentEssay:
		return c.Validate()
	default:
		return ErrInvalidChoiceID
	}
}

type isAnswerContent interface {
	isAnswerContent()
	Validate() error
}

type ContentEssay struct {
	ContentFormat format.Text `json:"content_format,omitempty"`
	Content       string      `json:"content,omitempty"`
}

func (c ContentEssay) isAnswerContent() {}

func (c ContentEssay) Validate() error {
	if !c.ContentFormat.Valid() {
		return ErrInvalidEssayFormat
	}

	if c.Content == "" {
		return ErrInvalidEssayContent
	}

	return nil
}

type ContentChoiceID int32

func (c ContentChoiceID) isAnswerContent() {}

func (c ContentChoiceID) Validate() error {
	if c == 0 {
		return ErrInvalidChoiceID
	}

	return nil
}

// Filter is a filter for listing questions.
type Filter struct {
	ParticipantID int32 `json:"participant_id,omitempty"`
	ExamID        int32 `json:"exam_id,omitempty"`
}

// ListOptions is a list options for listing questions with pagination.
type ListOptions struct {
	Filter Filter `json:"filter,omitempty"`
	Size   int32  `json:"size,omitempty"`
	Page   int32  `json:"limit,omitempty"`
}

func (o *ListOptions) Validate() error {
	if o.Size < -1 {
		return fmt.Errorf("invalid page size (%d < -1)", o.Size)
	}

	if o.Page < 1 {
		return fmt.Errorf("invalid page number (%d < 1)", o.Page)
	}

	return nil
}

// ListData is the data returned by the list operation.
type ListData struct {
	// Questions is the list of questions returned from the operation.
	Answers []Answer `json:"answers,omitempty"`
	// Total is the total number of questions available in the store.
	TotalSize int32 `json:"total_size,omitempty"`
}
