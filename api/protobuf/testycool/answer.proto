// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto3";

package testycool.v1;

import "google/api/annotations.proto";
import "google/protobuf/empty.proto";
import "testycool/text_format.proto";

option go_package = "gitlab.com/testycool/server/api/protobuf/gen/go/testycool";


// -----------------------------------------------------------------------------
// Service Definitions
// -----------------------------------------------------------------------------

service AnswerService {
  rpc GetAnswer(GetAnswerRequest) returns (GetAnswerResponse) {
    option (google.api.http) = {
      get: "/v1/answers/{id}"
    };
  }

  rpc ListAnswers(ListAnswersRequest) returns (ListAnswersResponse) {
    option (google.api.http) = {
      get: "/v1/answers"
    };
  }

  rpc CreateAnswer(CreateAnswerRequest) returns (CreateAnswerResponse) {
    option (google.api.http) = {
      post: "/v1/answers"
      body: "*"
    };
  }

  rpc UpdateAnswer(UpdateAnswerRequest) returns (UpdateAnswerResponse) {
    option (google.api.http) = {
      put: "/v1/answers"
      body: "*"
    };
  }

  rpc DeleteAnswer(DeleteAnswerRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/answers/{id}"
    };
  }
}


// -----------------------------------------------------------------------------
// Request and Response Definitions
// -----------------------------------------------------------------------------

message GetAnswerRequest {
  int32 id = 1;
}

message GetAnswerResponse {
  Answer answer = 1;
}

message ListAnswersRequest {
  int32 size = 1;
  int32 page = 2;
  message Filter {
    optional int32 participant_id = 2;
    optional int32 exam_id = 3;
  }
  Filter filter = 3;
}

message ListAnswersResponse {
  repeated Answer answers = 1;
  int32 size = 2;
  int32 page = 3;
  int32 total_size = 4;
}

message CreateAnswerRequest {
  int32 participant_id = 1;
  int32 question_id = 2;
  oneof content {
    int32 choice_id = 3;
    EssayAnswer essay = 4;
  }
}

message CreateAnswerResponse {
  Answer answer = 1;
}

message UpdateAnswerRequest {
  Answer answer = 1;
}

message UpdateAnswerResponse {
  Answer answer = 1;
}

message DeleteAnswerRequest {
  int32 id = 1;
}


// -----------------------------------------------------------------------------
// Resource Definitions
// -----------------------------------------------------------------------------

message Answer {
  int32 id = 1;
  int32 participant_id = 2;
  int32 question_id = 3;
  oneof content {
    int32 choice_id = 4;
    EssayAnswer essay = 5;
  }
  bool is_correct = 6;
}

message EssayAnswer {
  TextFormat format = 4;
  string content = 5;
}
